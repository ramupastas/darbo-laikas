-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2023 at 10:53 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `darbo_valandos`
--

-- --------------------------------------------------------

--
-- Table structure for table `darbo_vietos`
--

CREATE TABLE `darbo_vietos` (
  `id` bigint(20) NOT NULL,
  `darbo_vieta` varchar(50) NOT NULL,
  `komanda` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Dumping data for table `darbo_vietos`
--

INSERT INTO `darbo_vietos` (`id`, `darbo_vieta`, `komanda`) VALUES
(20, 'Pjovimo staklės Nr,1', 'Paruošėjai'),
(21, 'Surinkimo Stalas Nr.1', 'Surinikimas');

-- --------------------------------------------------------

--
-- Table structure for table `darbuotojai`
--

CREATE TABLE `darbuotojai` (
  `id` bigint(20) NOT NULL,
  `tabelio_id` bigint(20) NOT NULL,
  `darbuotojas` varchar(50) NOT NULL,
  `p1_pradzia` time NOT NULL,
  `p1_pabaiga` time NOT NULL,
  `pietus_pradzia` time NOT NULL,
  `pietus_pabaiga` time NOT NULL,
  `p2_pradzia` time NOT NULL,
  `p2_pabaiga` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Dumping data for table `darbuotojai`
--

INSERT INTO `darbuotojai` (`id`, `tabelio_id`, `darbuotojas`, `p1_pradzia`, `p1_pabaiga`, `pietus_pradzia`, `pietus_pabaiga`, `p2_pradzia`, `p2_pabaiga`) VALUES
(28, 123, 'Ramūnas Bytautas', '10:00:00', '10:15:00', '12:30:00', '13:00:00', '15:00:00', '15:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usersId` bigint(20) NOT NULL,
  `usersName` varchar(128) NOT NULL,
  `usersUid` varchar(128) NOT NULL,
  `usersPwd` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usersId`, `usersName`, `usersUid`, `usersPwd`) VALUES
(1, 'Ramūnas Bytautas', 'b.ramunas', '$2y$10$y17gbntVARNog8BvsR829erp0rvkodGn4YIK5XHmraXvFUxYRDaP6'),
(4, 'Vardas Pavarde', 'gerulis', '$2y$10$lgyAkKUfvcB8WTOIm.p6luqCw9Sm/qZU4yOXXjNiFJAl9ZIWFsYSW');

-- --------------------------------------------------------

--
-- Table structure for table `zurnalas`
--

CREATE TABLE `zurnalas` (
  `id` bigint(20) NOT NULL,
  `tabelio_id` bigint(20) NOT NULL,
  `darbuotojas` varchar(50) NOT NULL,
  `darbo_vieta` varchar(50) NOT NULL,
  `darbo_pradzia` datetime DEFAULT NULL,
  `darbo_pabaiga` datetime DEFAULT NULL,
  `redagavimo_laikas` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `trukme` float(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Dumping data for table `zurnalas`
--

INSERT INTO `zurnalas` (`id`, `tabelio_id`, `darbuotojas`, `darbo_vieta`, `darbo_pradzia`, `darbo_pabaiga`, `redagavimo_laikas`, `trukme`) VALUES
(171, 123, 'Ramūnas Bytautas', 'Pjovimo staklės Nr,1', '2023-05-15 11:27:00', '2023-05-15 11:27:06', '2023-05-15 08:27:06', 0),
(172, 123, 'Ramūnas Bytautas', 'Surinkimo Stalas Nr.1', '2023-05-15 11:27:41', '2023-05-15 11:30:06', '2023-05-15 08:30:06', 0),
(173, 123, 'Ramūnas Bytautas', 'Surinkimo Stalas Nr.1', '2023-05-15 11:30:13', NULL, '2023-05-15 08:30:13', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `darbo_vietos`
--
ALTER TABLE `darbo_vietos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `darbuotojai`
--
ALTER TABLE `darbuotojai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usersId`);

--
-- Indexes for table `zurnalas`
--
ALTER TABLE `zurnalas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tabelio_id` (`tabelio_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `darbo_vietos`
--
ALTER TABLE `darbo_vietos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `darbuotojai`
--
ALTER TABLE `darbuotojai`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usersId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `zurnalas`
--
ALTER TABLE `zurnalas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
