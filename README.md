# Paskirtis

- Ši pograma sukurta vidiniam gamybinės įmonės naudojimui - eksperimentui. Programa naudojama suskaičiuoti kiek laiko darbuotojai išdirbo prie konkrečios darbo vietos išsaugant informaciją duomenų bazėje. Vėliau duomenis galima peržiūrėti programos lange arba eksportuoti CSV failą ir analizuoti Excel pagalba.
- Šio eksperimentavimo tikslas - geriau suprasti būsimos gamybos valdymo programos poreikius ir pratinti gamybos žmones naudoti kompiuterius darbo vietose, reguliariai vesti į juos informaciją.

# Proceso esmė

- Pradedant darbą darbuotojas įveda savo tabelio numerį (bar-kodas arba klaviatura), pasirenka darbo vietą ir spaudžia pradžios mygtuką. Nuo to momento skaičiuojams darbo laikas.
- Jeigu darbuotojas turi registruotis naujoje darbo vietoje - programa reikalauja užbaigti prieš tai pradėtą darbą.
- Darbo pabaigoje darbuotojas turi užbaigti darbą pats. Jeigu jis to nepadaro, po nustatyto laiko (30 min.) programa automatiškai užbaigia darbą ir įrašo standartį darbo pabaigos laiką.
- Jeigu dirbami viršvalandžiai, meistras turi redaguoti programoje darbo pabaigos laiką. Tam yra sukurta satskiras programos langas kuriame matomas visų darbuotojų darbų sąrašas.
- Programa automatiškai iš darbo laiko išminusuoja darbuotojų pertraukų ir pietų laiką. Meistras turi kiekvienam darbuotojui programoje aprašyti standartinius pertraukų ir pietų laikus.
- Meistras visus veiksmus atlieka administravimo panelėje prie kurios prisijungia savo vartotojo vardu ir slaptažodžiu.

# Programos kūrimo eiga

- Programą sukurta naudojant PHP, HTML, Bootsrap, Javascript ir MariaDB.
- Įmonėje atsiradus poreikiui turėti parastą įrankį skaičiuoti darbo valandas, kuris būtų efektyvesnis nei Excel lentelės, nusprendėme pabandyti savo jėgomis jį pasidaryti.
- Pasižiūrint Youtube pamokėles, po truputį gavosi primityvi, bet pilnai veikianti programa.
