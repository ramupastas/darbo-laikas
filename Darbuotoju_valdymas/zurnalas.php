<?php
session_start();

if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}


include "includes/ispejimai.inc.php";
include "includes/dbh.inc.php";


// suformuojamos default datos input name='from_date' ir name='to_date'
$atgal = new \DateTime('-1 month');
$nuo_datos = $atgal->format('Y-m-d');

$rytoj = new \DateTime('+1 day');
$iki_datos = $rytoj->format('Y-m-d');

?>

<!DOCTYPE html>
<html lang="lt">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <title>Darbuotojų registracijos žurnalas</title>
</head>

<body>


    <nav class=" navbar navbar-light justify-content-center fs-3 ">
        <h3>Darbo laiko registracijos žurnalas </h3>

        <a class="btn btn-outline-primary btn-sm ml-4" href="includes/export_csv.inc.php">Export CSV</a>
        <a class="btn btn-outline-primary btn-sm ml-4" href="printTable.php">Spausdinimas</a>
        <a class="btn btn-outline-success btn-sm ml-4" href="index.php">Pagrindinis Meniu</a>
        <a class="btn btn-outline-dark btn-sm ml-4" href="includes/logout.inc.php">Atsijungti</a>

    </nav>
    <?php
    echo $signalas; //is ispejimai.php
    ?>
    <div class="container justify-content-center">
        <div class="row mb-3 input-group-text">
            <form action="" method="GET">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nuo Datos</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?= $nuo_datos ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Iki Datos</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?= $iki_datos ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label></label> <br>
                            <button class="btn btn-info" type="submit">Rodyti pažymėtą laikotarpį</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <input type="text" class="form-control" id="live_search" name="search" autocomplete="off" placeholder="Ieškoti pagal darbuotoją....">
                        </div>

                    </div>

                </div>
            </form>
        </div>
    </div>


    <div id="search_result"></div>

    <div class="mano_lenta container" id="default_result">
        <table id="myTable" class="table table-hover table-striped text-center">
            <thead class="table-primary">
                <tr>
                    <th scope="col">Tabelio numeris</th>
                    <th scope="col">Darbuotojas</th>
                    <th scope="col">Darbo vieta</th>
                    <th scope="col">Darbo pradzia</th>
                    <th scope="col">Darbo pabaiga</th>
                    <th scope="col">Trukmė, val.</th>
                    <th scope="col">Redagavimas</th>
                    <th scope="col">Redaguota</th>

                </tr>
            </thead>
            <tbody class="table-light">
                <?php
                //Filtruojamas sarasas. kad rodytu tarp ivestu datu
                if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
                    $from_date = $_GET['from_date'];
                    $to_date = $_GET['to_date'];

                    $sql = "SELECT * FROM `zurnalas`WHERE darbo_pradzia BETWEEN '$from_date' AND '$to_date' ORDER BY darbo_pradzia DESC";
                } else {
                    // Jei $_GET tuscias - rodo nefiltruota darbu sarasa
                    $sql = "SELECT * FROM `zurnalas` ORDER BY darbo_pradzia DESC LIMIT 100";
                }


                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {

                    while ($row = mysqli_fetch_assoc($result)) {

                        $pradzia = $row['darbo_pradzia'];
                        $pabaiga = $row['darbo_pabaiga'];

                        $trukme = round((strtotime($pabaiga) - strtotime($pradzia)) / 3600, 2);
                        $trukme = $trukme < 0 ? "" : $trukme;

                        $pradzia = date("y-m-d H:i", strtotime($row['darbo_pradzia']));
                        $pabaiga = $trukme < 0 ? "" : date("y-m-d H:i", strtotime($row['darbo_pabaiga']));
                        $paskutinis_redagavimas = date("m-d H:i", strtotime($row['redagavimo_laikas']));

                        $trukme = round($row['trukme'] / 3600, 4); // $trukme DB saugoma sekundemis
                ?>
                        <tr id="sarasas">
                            <td><?php echo $row['tabelio_id'] ?></td>
                            <td><?php echo $row['darbuotojas'] ?></td>
                            <td><?php echo $row['darbo_vieta'] ?></td>
                            <td><?php echo $pradzia; ?></td>
                            <td><?php echo $pabaiga; ?></td>
                            <td><?php echo $trukme; ?></td>



                            <td>
                                <a class="redaguoti btn btn-outline-primary btn-sm" href="includes/edit.inc.php?id=<?php echo $row['id']; ?>">
                                    <!-- <i class="fa-solid fa-pen-to-square fs-5 me3"></i> -->
                                    Redaguoti
                                </a>


                            </td>
                            <td><?php echo $paskutinis_redagavimas; ?></td>

                        </tr>
                <?php
                    }
                }
                ?>


            </tbody>
        </table>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="js\timer.js"></script>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <script src="js/livesearch.js"></script>



</body>

</html>