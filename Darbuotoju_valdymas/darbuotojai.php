<?php
session_start();

if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}


include "includes/ispejimai.inc.php";
include "includes/dbh.inc.php";


?>

<!DOCTYPE html>
<html lang="lt">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <title>PVC cecho darbuotojai</title>
</head>

<body>


    <nav class=" navbar navbar-light justify-content-center fs-3 ">
        <a class="btn btn-success btn-sm mr-5" href="includes/nauj_darbuotojas.inc.php">Naujas Darbuotojas</a>
        <h4>PVC cecho darbuotojai </h4>
        <a class="btn btn-outline-success btn-sm ml-4" href="index.php">Pagrindinis Meniu</a>
        <a class="btn btn-outline-dark btn-sm ml-4" href="includes/logout.inc.php">Atsijungti</a>

    </nav>
    <?php
    echo $signalas; //is ispejimai.php
    ?>


    <div class="mano_lenta container" id="default_result">
        <table id="myTable" class="table table-hover table-striped text-center" style="width:120%; position: relative; left: -130px;">
            <thead class="table-primary">
                <tr>
                    <th scope="col">Tabelio numeris</th>
                    <th scope="col">Darbuotojas</th>
                    <th scope="col">P1 nuo</th>
                    <th scope="col">P1 iki</th>
                    <th scope="col">Pietūs nuo</th>
                    <th scope="col">Pietūs iki.</th>
                    <th scope="col">P2 nuo</th>
                    <th scope="col">P2 iki</th>
                    <th scope="col">Veiksmai</th>

                </tr>
            </thead>
            <tbody class="table-light">
                <?php

                // Jei $_GET tuscias - rodo nefiltruota darbu sarasa
                $sql = "SELECT * FROM `darbuotojai` ORDER BY darbuotojas ASC";

                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {

                    while ($row = mysqli_fetch_assoc($result)) {
                        $tabelio_id = $row['tabelio_id'];
                        $darbuotojas = $row['darbuotojas'];
                        $p1_nuo = $row['p1_pradzia'];
                        $p1_iki = $row['p1_pabaiga'];
                        $pietus_nuo = $row['pietus_pradzia'];
                        $pietus_iki = $row['pietus_pabaiga'];
                        $p2_nuo = $row['p2_pradzia'];
                        $p2_iki = $row['p2_pabaiga'];

                ?>
                        <tr id="sarasas">
                            <td><?php echo $tabelio_id; ?></td>
                            <td><?php echo $darbuotojas; ?></td>
                            <td><?php echo $p1_nuo; ?></td>
                            <td><?php echo $p1_iki; ?></td>
                            <td><?php echo $pietus_nuo; ?></td>
                            <td><?php echo $pietus_iki; ?></td>
                            <td><?php echo $p2_nuo; ?></td>
                            <td><?php echo $p2_iki; ?></td>



                            <td>
                                <a class="redaguoti btn btn-outline-primary btn-sm" href="includes/edit_darbuo.inc.php?id=<?php echo $row['id']; ?>">
                                    <!-- <i class="fa-solid fa-pen-to-square fs-5 me3"></i> -->
                                    Redaguoti
                                </a>


                            </td>


                        </tr>
                <?php
                    }
                }
                ?>


            </tbody>
        </table>

    </div>

    <script src="js\timer.js"></script>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>




</body>

</html>