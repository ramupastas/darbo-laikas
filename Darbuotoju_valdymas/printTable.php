<?php
session_start();

if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}

include "includes/dbh.inc.php";


// suformuojamos default datos input name='from_date' ir name='to_date'
$atgal = new \DateTime('- 1 day');
$nuo_datos = $atgal->format('Y-m-d');

$siandien = new \DateTime();
$iki_datos = $siandien->format('Y-m-d');

?>

<!DOCTYPE html>
<html lang="lt">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link id="btsrp" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <title>Darbo laiko atskaita</title>
</head>

<body>


    <nav class=" navbar navbar-light justify-content-center fs-3">
        <h3 class="fs-5">Darbo laiko ataskaita </h3>

        <a class="btn btn-outline-success ml-4" href="index.php">Pagrindinis Meniu</a>
        <a class="btn btn-outline-danger ml-4" href="includes/logout.inc.php">Atsijungti</a>
        <button id="printBTN" class="btn btn-primary ml-4">Spausdinti</button>

    </nav>

    <div class="container justify-content-center">
        <div class="row mb-3 input-group-text">
            <form action="" method="GET">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Data</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?= $nuo_datos ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label></label> <br>
                            <button class="btn btn-secondary" type="submit">Rodyti pažymėtą laikotarpį</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label hidden>Iki Datos</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?= $iki_datos ?>" hidden>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">

                            <input hidden type="text" class="form-control" id="live_search" name="search" autocomplete="off" placeholder="Ieškoti pagal darbuotoją....">
                        </div>

                    </div>

                </div>
            </form>
        </div>
    </div>


    <div id="search_result"></div>

    <div class="mano_lenta container" id="default_result">
        <link id="btsrp" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">

        <table id="myTable" class="table text-center table-bordered" style="font-size: 12px;">
            <col style="width: 5%;">
            <col style="width: 15%;">
            <col style="width: 15%;">
            <col style="width: 10%;">
            <col style="width: 10%;">
            <col style="width: 8%;">
            <thead class="table-light">
                <tr>
                    <th scope="col">Tabelio numeris</th>
                    <th scope="col">Darbuotojas</th>
                    <th scope="col">Darbo vieta</th>
                    <th scope="col">Darbo pradzia</th>
                    <th scope="col">Darbo pabaiga</th>
                    <th scope="col">Trukmė, val.</th>
                    <th scope="col">Komentaras</th>


                </tr>
            </thead>
            <tbody class="table">
                <?php
                //Filtruojamas sarasas. kad rodytu tarp ivestu datu
                if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
                    $from_date = $_GET['from_date'];
                    $to_date = $_GET['to_date'];

                    // $sql = "SELECT tabelio_id, darbuotojas, darbo_vieta, darbo_pradzia, darbo_pabaiga SUM(trukme) FROM `zurnalas` GROUP BY darbuotojas HAVING DATE (darbo_pradzia) = '$from_date'";

                    $sql = "SELECT * FROM `zurnalas` WHERE DATE (darbo_pradzia) = '$from_date' ORDER BY darbuotojas ASC";
                } else {
                    // Jei $_GET tuscias - rodo nefiltruota darbu sarasa
                    $sql = "SELECT * FROM `zurnalas` ORDER BY darbo_pradzia DESC LIMIT 100";
                }


                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {

                    while ($row = mysqli_fetch_assoc($result)) {

                        $pradzia = $row['darbo_pradzia'];
                        $pabaiga = $row['darbo_pabaiga'];

                        $trukme = round((strtotime($pabaiga) - strtotime($pradzia)) / 3600, 2);
                        $trukme = $trukme < 0 ? "" : $trukme;

                        $pradzia = date("y-m-d H:i", strtotime($row['darbo_pradzia']));
                        $pabaiga = $trukme < 0 ? "" : date("y-m-d H:i", strtotime($row['darbo_pabaiga']));


                        $trukme = round($row['trukme'] / 3600, 1); // $trukme DB saugoma sekundemis
                ?>
                        <tr id="sarasas">
                            <td><?php echo $row['tabelio_id'] ?></td>
                            <td><?php echo $row['darbuotojas'] ?></td>
                            <td><?php echo $row['darbo_vieta'] ?></td>
                            <td><?php echo $pradzia; ?></td>
                            <td><?php echo $pabaiga; ?></td>
                            <td><?php echo $trukme; ?></td>
                            <td></td>
                        </tr>
                <?php
                    }
                }
                ?>


            </tbody>
        </table>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="js\timer.js"></script>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>


    <script src="js/printContent.js"></script>


</body>

</html>