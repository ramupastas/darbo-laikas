<?php
session_start();
if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}

require_once "dbh.inc.php";
require_once "functions.inc.php";

if (isset($_GET['id'])) {

    $id = $_GET['id'];
    deleteFromZurnalas($conn, $id);
}
