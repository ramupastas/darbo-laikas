<?php
session_start();
if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}

include_once "dbh.inc.php";
require_once "functions.inc.php";

if (isset($_POST['submit'])) {

    $tabelio_id = $_POST['tabelio_id'];
    $darbuotojas = $_POST['darbuotojas'];
    $p1_nuo = $_POST['p1_nuo'];
    $p1_iki = $_POST['p1_iki'];
    $pietus_nuo = $_POST['pietus_nuo'];
    $pietus_iki = $_POST['pietus_iki'];
    $p2_nuo = $_POST['p2_nuo'];
    $p2_iki = $_POST['p2_iki'];

    createDarbuotojas($conn, $tabelio_id, $darbuotojas, $p1_nuo, $p1_iki, $pietus_nuo, $pietus_iki, $p2_nuo, $p2_iki);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- Font awesome -->
    <title>Document</title>
</head>

<body>
    <nav class="navbar navbar-light justify-content-center fs-3 mb-5">
        <h3>Sukurti naują įrašą</h3>
    </nav>

    <div class="container d-flex justify-content-center input-group-text" style="padding:30px ;">
        <form action="" method="post" style="width:50vw; min-width:300px;">

            <div class="row mb-3">

                <div class="col">
                    <label class="form-label">Tabelio Numeris:</label>
                    <input type="text" class="form-control" name="tabelio_id" value="">
                </div>
                <div class="col">
                    <label class="form-label">Darbuotojas:</label>
                    <input type="text" class="form-control" name="darbuotojas" style="font-size: larger; font-weight:bolder" value="">
                </div>
            </div>

            <div class="row mb-3 mt-3">
                <div class="col mb-3 mt-3">
                    <label class="form-label">P1 nuo:</label>
                    <input type="datetime" class="form-control" name="p1_nuo" value="">
                </div>
                <div class="col mb-3 mt-3">
                    <label class="form-label">P1 iki:</label>
                    <input type="datetime" class="form-control" name="p1_iki" value="">
                </div>
            </div>
            <div class="row mb-3 mt-3">
                <div class="col mb-3 mt-3">
                    <label class="form-label">Pietūs nuo:</label>
                    <input type="datetime" class="form-control" name="pietus_nuo" value="">
                </div>
                <div class="col mb-3 mt-3">
                    <label class="form-label">Pietūs iki:</label>
                    <input type="datetime" class="form-control" name="pietus_iki" value="">
                </div>
            </div>

            <div class="row mb-3 mt-3">
                <div class="col mb-3 mt-3">
                    <label class="form-label">P2 nuo:</label>
                    <input type="datetime" class="form-control" name="p2_nuo" value="">
                </div>
                <div class="col mb-3 mt-3">
                    <label class="form-label">P2 iki:</label>
                    <input type="datetime" class="form-control" name="p2_iki" value="">
                </div>
            </div>


            <div class="d-grid gap-4 d-md-flex justify-content-md-center">
                <button type="submit" class="btn btn-outline-success" name="submit" value="submit">Įrašyti</button>

                <a href="../darbuotojai.php" class="btn btn-outline-danger">Cancel</a>
            </div>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>