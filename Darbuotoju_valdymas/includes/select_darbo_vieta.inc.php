<?php

include_once "dbh.inc.php";

// Is duomenu bazes lentos darbo_vietos i index.php <select> id="darbo-vietos" irasome darbo vietu pavadinimus
$sql = "SELECT DISTINCT darbo_vieta FROM `darbo_vietos`";

$result = mysqli_query($conn, $sql);

$option = "";

while ($row = mysqli_fetch_assoc($result)) {
    $option .= "<option class='form-select'  value='{$row['darbo_vieta']}'>{$row['darbo_vieta']}</option>";
}
