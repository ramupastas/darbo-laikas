<?php
session_start();
if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}

include_once "dbh.inc.php";
include_once "functions.inc.php";

if (isset($_GET['from_date']) && isset($_GET['to_date'])) {

    $from_date = $_GET['from_date'];
    $to_date = $_GET['to_date'];
    $info_text = "Pasirinkta Nuo: " . $from_date . " iki " . $to_date;

    $csv_export = exportCSV($conn, $from_date, $to_date);
} else {
    $csv_export = "";
    $info_text = "Pasirinkite datas ir spauskite \"Laikotarpis\"";
}

$menuo_atgal = new DateTime('-1 month');
$nuo_datos = $menuo_atgal->format('Y-m-d');

$diena_pirmyn = new DateTime('+1 day');
$iki_datos = $diena_pirmyn->format('Y-m-d');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Eport To CSV</title>
</head>

<body>
    <div class="container p-5" style="background-color:#edede9; width:40vw; min-width:300px;">
        <form method="GET">
            <div class=" col m-4">
                <div class="form-group">
                    <label>Nuo Datos</label>
                    <input type="date" name="from_date" id="from_date" class="form-control" value="<?= $nuo_datos ?>">
                </div>
            </div>
            <div class="col m-4">
                <div class="form-group">
                    <label>Iki Datos</label>
                    <input type="date" name="to_date" id="to_date" class="form-control" value="<?= $iki_datos ?>">
                </div>
            </div>

            <h5><?php echo $info_text ?></h5>

            <a class="btn btn-secondary m-4" href="../zurnalas.php">Atgal</a>
            <input class="btn btn-info m-4" type="submit" value="Laikotarpis">

            <?php
            if ($csv_export != "") {

                $timestamp = new DateTime('now');
                $file_name = $timestamp->format('ymdHms') . "zurnalas.csv";

                echo '<a class="btn btn-success m-4" href="' . $csv_export . '"download="' . $file_name . '" >Export CSV</a>';
            } else {
                exit();
            }

            ?>
        </form>
        <?php echo "Failo pavadinimas: " . $file_name; ?>
    </div>
</body>

</html>