<?php
session_start();

if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}
include_once "dbh.inc.php";

?>

<html>
<div class="mano_lenta container" id="defaultresult">
    <table id="myTable" class="table table-hover table-striped text-center">
        <thead class="table-primary">
            <tr>
                <th scope="col">Tabelio numeris</th>
                <th scope="col">Darbuotojas</th>
                <th scope="col">Darbo vieta</th>
                <th scope="col">Darbo pradzia</th>
                <th scope="col">Darbo pabaiga</th>
                <th scope="col">Trukmė, val.</th>
                <th scope="col">Redagavimas</th>
                <th scope="col">Redaguota</th>

            </tr>
        </thead>
        <tbody class="table-light">
            <?php
            //Filtruojamas sarasas. kad rodytu tarp ivestu datu
            if (isset($_POST['input'])) {

                $input = $_POST['input'];

                $sql = "SELECT * FROM `zurnalas`WHERE darbuotojas LIKE '%{$input}%' ORDER BY darbo_pradzia DESC";
            }

            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {

                while ($row = mysqli_fetch_assoc($result)) {

                    $pradzia = $row['darbo_pradzia'];
                    $pabaiga = $row['darbo_pabaiga'];

                    $trukme = round((strtotime($pabaiga) - strtotime($pradzia)) / 3600, 2);
                    $trukme = $trukme < 0 ? "" : $trukme;

                    $pradzia = date("y-m-d H:i", strtotime($row['darbo_pradzia']));
                    $pabaiga = $trukme < 0 ? "" : date("y-m-d H:i", strtotime($row['darbo_pabaiga']));
                    $paskutinis_redagavimas = date("m-d H:i", strtotime($row['redagavimo_laikas']));
                    $trukme = round($row['trukme'] / 3600, 4); // $trukme DB saugoma sekundemis
            ?>
                    <tr id="sarasas">
                        <td><?php echo $row['tabelio_id'] ?></td>
                        <td><?php echo $row['darbuotojas'] ?></td>
                        <td><?php echo $row['darbo_vieta'] ?></td>
                        <td><?php echo $pradzia; ?></td>
                        <td><?php echo $pabaiga; ?></td>
                        <td><?php echo $trukme; ?></td>



                        <td>
                            <a class="redaguoti btn btn-outline-primary btn-sm" href="includes/edit.inc.php?id=<?php echo $row['id']; ?>">
                                <!-- <i class="fa-solid fa-pen-to-square fs-5 me3"></i> -->
                                Redaguoti
                            </a>


                        </td>
                        <td><?php echo $paskutinis_redagavimas; ?></td>

                    </tr>
            <?php
                }
            }
            ?>


        </tbody>
    </table>

</div>

</html>