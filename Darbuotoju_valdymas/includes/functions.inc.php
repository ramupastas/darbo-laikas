<?php

function emptyInputSignup($name, $username, $pwd, $pwdRepeat)
{

    if (empty($name) || empty($username) || empty($pwd) || empty($pwdRepeat)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function invalidUid($username)
{

    if (!preg_match("/^[.a-zA-Z0-9]*$/", $username)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function pwdMatch($pwd, $pwdRepeat)
{
    if ($pwd !== $pwdRepeat) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function uidExists($conn, $username)
{
    $sql = "SELECT * FROM users WHERE usersUid = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfailed");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $username);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        return $row;
    } else {
        $result = false;
        return $result;
    }
    mysqli_stmt_close($stmt);
}

function createUser($conn, $name, $username, $pwd)
{
    $sql = "INSERT INTO users (usersName, usersUid, usersPwd) VALUES (?,?,?);";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfailed");
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "sss", $name, $username, $hashedPwd);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    header("location: ../index.php?error=none");
    exit();
}

function createDarbuotojas($conn, $tabelio_id, $darbuotojas, $p1_nuo, $p1_iki, $pietus_nuo, $pietus_iki, $p2_nuo, $p2_iki)
{
    $sql = "INSERT INTO `darbuotojai` (tabelio_id, darbuotojas, p1_pradzia, p1_pabaiga, pietus_pradzia, pietus_pabaiga, p2_pradzia,p2_pabaiga ) VALUES (?,?,?,?,?,?,?,?);";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../nauj_darbuotojas.inc.php?error=stmtfailed");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "isssssss", $tabelio_id, $darbuotojas, $p1_nuo, $p1_iki, $pietus_nuo, $pietus_iki, $p2_nuo, $p2_iki);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    header("location: ../darbuotojai.php?success_msg=Naujas darbuotojas sukurtas");
    exit();
}

function emptyInputLogin($username, $pwd)
{
    if (empty($username) || empty($pwd)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function loginUser($conn, $username, $pwd)
{
    $uidExists = uidExists($conn, $username, $username);

    if ($uidExists == false) {

        header("location: ../login.php?error=wronglogin");
        exit();
    }
    $pwdHashed = $uidExists['usersPwd'];
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false) {
        header("location: ../login.php?error=wronglogin");
        exit();
    } elseif ($checkPwd === true) {
        session_start();
        $_SESSION['userid'] = $uidExists['usersId'];
        $_SESSION['useruid'] = $uidExists['usersUid'];

        header("location: ../index.php");
        exit();
    }
}

function deleteFromZurnalas($conn, $id)
{

    $sql = "DELETE FROM `zurnalas` WHERE id = ? ";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../zurnalas.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    header("location:../zurnalas.php?fail_msg=Įrašas panaikintas");
    exit();
}

function deleteFromDarbuo($conn, $id)
{

    $sql = "DELETE FROM `darbuotojai` WHERE id = ? ";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../darbuotojai.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    header("location:../darbuotojai.php?fail_msg=Įrašas panaikintas");
    exit();
}

function openEdit($conn, $id)
{
    $sql = "SELECT * FROM `zurnalas` WHERE id = ? LIMIT 1";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../zurnalas.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);
    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        return $row;
    } else {
        return false;
    }
    mysqli_stmt_close($stmt);
}

function openEditDarbuo($conn, $id)
{
    $sql = "SELECT * FROM `darbuotojai` WHERE id = ? LIMIT 1";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../darbuotojai.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);
    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        return $row;
    } else {
        return false;
    }
    mysqli_stmt_close($stmt);
}

function editDarbas($conn, $darbo_vieta, $darbo_pradzia, $darbo_pabaiga, $darbo_trukme, $id)
{

    $darbo_trukme = ($darbo_trukme > 0) ? $darbo_trukme : 0;

    $sql = "UPDATE `zurnalas` SET darbo_vieta = ?,darbo_pradzia = ?, darbo_pabaiga = ?, trukme =? WHERE id = ?";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../zurnalas.php?warning_msg=SQL Klaida");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "sssdi", $darbo_vieta, $darbo_pradzia, $darbo_pabaiga, $darbo_trukme, $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../zurnalas.php?success_msg=Įrašas atnaujintas");
    exit();
}

function editDarbuotojai($conn, $p1_nuo, $p1_iki, $pietus_nuo, $pietus_iki, $p2_nuo, $p2_iki, $id)
{

    $sql = "UPDATE `darbuotojai` SET p1_pradzia = ?, p1_pabaiga = ?, pietus_pradzia = ?, pietus_pabaiga = ?, p2_pradzia = ?, p2_pabaiga = ? WHERE id = ?";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../zurnalas.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ssssssi", $p1_nuo, $p1_iki, $pietus_nuo, $pietus_iki, $p2_nuo, $p2_iki, $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../darbuotojai.php?success_msg=Įrašas atnaujintas");
    exit();
}

function exportCSV($conn, $from_date, $to_date)
{
    $sql = "SELECT * FROM `zurnalas`WHERE darbo_pradzia BETWEEN ? AND ?";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../zurnalas.php?warning_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss", $from_date, $to_date);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    // Duomenu irasymas i CSV faila
    $csv_data = "";

    while ($row = mysqli_fetch_assoc($resultData)) {
        $csv_data .= $row['id'] . ',' . $row['tabelio_id'] . ',' . $row['darbuotojas'] . ',' . $row['darbo_vieta'] . ',' . $row['darbo_pradzia'] . ',' . $row['darbo_pabaiga']. ',' . $row['trukme'] . "\n";
    }
    $csv_export = "data:text/csv;charset=utf-8,id,tabelio_nr,darbuotojas,darbo_vieta,darbo_pradzia,darbo_pabaiga,trukme_sekund\n";
    $csv_export .= $csv_data;
    return $csv_export;
    mysqli_stmt_close($stmt);
}
