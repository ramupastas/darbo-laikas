<?php
session_start();
if (!isset($_SESSION['useruid'])) {
    header("location:login.php");
    exit();
}

include_once "dbh.inc.php";
include_once "select_darbo_vieta.inc.php";
require_once "functions.inc.php";
date_default_timezone_set('Europe/Vilnius');

if (isset($_GET['id'])) {

    $id = $_GET['id'];
    $row = openEdit($conn, $id);
} else {
    header("Location: ../zurnalas.php");
}

if (isset($_POST['submit'])) {

    $darbo_vieta = $_POST['darbo_vietos'];
    $tabelio_id = $_POST['tabelio_id'];
//************* paskaiciuojama standartine darbo pradzia, pabaiga ir trukme sekundemis. Neskaiciuojant pertrauku*/
    $darbo_pradzia = strtotime($_POST['darbo_pradzia']);
    $darbo_pabaiga = strtotime($_POST['darbo_pabaiga']);
    $darbo_trukme = $darbo_pabaiga - $darbo_pradzia;
   
//************Pertrauku pradziu ir pabaigu paskaiciavimas darbo vykdymo dienai */
    $sql = "SELECT * FROM `darbuotojai` WHERE tabelio_id = $tabelio_id LIMIT 1";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    $p1_nuo = strtotime(date("Y-m-d",$darbo_pradzia) . " " . $row['p1_pradzia']);
    $p1_iki = strtotime(date("Y-m-d",$darbo_pradzia) . " " . $row['p1_pabaiga']);

    $pietus_nuo = strtotime(date("Y-m-d",$darbo_pradzia) . " "  . $row['pietus_pradzia']);
    $pietus_iki = strtotime(date("Y-m-d",$darbo_pradzia) . " "  . $row['pietus_pabaiga']);

    $p2_nuo = strtotime(date("Y-m-d",$darbo_pradzia) . " "  . $row['p2_pradzia']);
    $p2_iki = strtotime(date("Y-m-d",$darbo_pradzia) . " "  . $row['p2_pabaiga']);

    //****************Minusuojamos  pertraukos iš darbo trukmes sekundemis */
    if ($p1_nuo > $darbo_pradzia && $p1_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($p1_iki - $p1_nuo);
      
    }
    if ($pietus_nuo > $darbo_pradzia && $pietus_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($pietus_iki - $pietus_nuo);
       
    }
    if ($p2_nuo > $darbo_pradzia && $p2_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($p2_iki - $p2_nuo);
      
    }

    $darbo_pradzia = $_POST['darbo_pradzia'];
    $darbo_pabaiga = $_POST['darbo_pabaiga'];
   
 
// $trukme i DB rasoma sekundemis
    editDarbas($conn, $darbo_vieta, $darbo_pradzia, $darbo_pabaiga, $darbo_trukme, $id);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>Document</title>
</head>

<body>
    <nav class="navbar navbar-light justify-content-center fs-3 mb-5">
        <h3>Redaguoti įrašą</h3>
    </nav>

    <div class="container d-flex justify-content-center input-group-text" style="padding:30px ;">
        <form action="" method="post" style="width:50vw; min-width:300px;">

            <div class="row mb-3">
                <div class="col">
                    <label class="form-label">Tabelio Numeris:</label>
                    <input type="text" class="form-control" name="tabelio_id" value="<?php echo $row['tabelio_id']; ?>" readonly>
                </div>
                <div class="col">
                    <label class="form-label">Darbuotojas:</label>
                    <input type="text" class="form-control" name="darbuotojas" value="<?php echo $row['darbuotojas']; ?>" readonly>
                </div>
            </div>

            <div class="row mb-3 mt-3">
                <label class="form-label">Darbo Vieta:</label>
                <select class="form-select" id="darbo_vietos" name="darbo_vietos">
                    <option value="<?php echo $row['darbo_vieta']; ?>" selected><?php echo $row['darbo_vieta']; ?></option>
                    <?php
                    echo $option; //is select_darbo_vieta.php
                    ?>
                </select>

                <div class="col mb-3 mt-3">
                    <label class="form-label">Darbo Pradžia:</label>
                    <input type="datetime" class="form-control" name="darbo_pradzia" value="<?php echo $row['darbo_pradzia']; ?>">
                </div>
                <div class="col mb-3 mt-3">
                    <label class="form-label">Darbo Pabaiga</label>
                    <input type="datetime" class="form-control" name="darbo_pabaiga" value="<?php echo $row['darbo_pabaiga']; ?>">
                </div>
            </div>

            <div class="d-grid gap-4 d-md-flex justify-content-md-center">
                <button type="submit" class="btn btn-success" name="submit" value="submit">Atnaujinti</button>
                <a href="../zurnalas.php" class="btn btn-warning mr-5">Cancel</a>
                <a href="delete.inc.php?id=<?= $id ?>" class="btn btn-danger ml-5">Panaikinti įrašą</a>
            </div>
        </form>
    </div>






    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>