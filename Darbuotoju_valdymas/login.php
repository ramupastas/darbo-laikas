<?php
include_once "header.php";
?>

<div class="wrapper m-2 p-4">
    <h4>
        Prisijungti
    </h4>
    <?php
    if (isset($_GET['error'])) {
        if ($_GET['error'] == "emptyinput") {
            echo "<p>Uzpildykite visus laukelius</p>";
        } elseif ($_GET['error'] == "wronglogin") {
            echo "<p>Neteisingi prisijungimo duomenys</p>";
        }
    }

    ?>
    <form action="includes/login.inc.php" method="post">
        <div class="mb-3 mt-3">
            <input type="text" class="form-control" name="uid" placeholder="Vartotojo vardas...">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control" name="pwd" placeholder="Slaptažodis...">
        </div>


        <button type="submit" class="btn btn-primary" name="submit">Prisijungti</button>
    </form>

</div>