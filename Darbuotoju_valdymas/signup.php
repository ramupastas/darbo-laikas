<?php
include_once "header.php";
?>

<div class="wrapper m-2 p-4">
    <h4>
        Registruoti vartotoją
    </h4>
    <?php
    if (isset($_GET['error'])) {
        if ($_GET['error'] == "emptyinput") {
            echo "<p>Uzpildykite visus laukelius</p>";
        } elseif ($_GET['error'] == "invaliduid") {
            echo "<p>Vartotojo vardas turi neleistinu simboliu</p>";
        } elseif ($_GET['error'] == "pwddontmatch") {
            echo "<p>Slaptazodziai neveinodi</p>";
        } elseif ($_GET['error'] == "usernametaken") {
            echo "<p>Vartotojas jau yra toks</p>";
        } elseif ($_GET['error'] == "stmtfailed") {
            echo "<p>Technies problemos STMT</p>";
        } elseif ($_GET['error'] == "none") {
            echo "<p>registracija sekminga</p>";
        }
    }

    ?>
    <form action="includes/signup.inc.php" method="post">
        <div class="mb-3 mt-3">
            <input type="text" class="form-control" name="name" placeholder="Vardas Pavardė...">
        </div>
        <div class="mb-3">
            <input type="text" class="form-control" name="uid" placeholder="Vartotojo vardas...">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control" name="pwd" placeholder="Slaptažodis...">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control" name="pwdrepeat" placeholder="Pakartoti slaptaždį...">
        </div>

        <button type="submit" class="btn btn-primary" name="submit">Užregistruoti</button>
    </form>

</div>