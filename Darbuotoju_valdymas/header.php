<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="css\reset.css">
    <link rel="stylesheet" href="css\style.css">
    <title>Darbuotojai</title>
</head>

<body>
    <div class="p-1">

        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">

                        <?php
                        if (isset($_SESSION['useruid'])) {
                            // echo '<li class="nav-item"><a class="nav-link active" href="index.php">Home</a></li>';
                            echo '<li class="nav-item"><a class="nav-link active" href="zurnalas.php" style="color:#162073;" >Žurnalas</a></li>';
                            echo '<li class="nav-item"><a class="nav-link active" href="printTable.php" style="color:#162073;" >Spausdinimas</a></li>';
                            echo '<li class="nav-item"><a class="nav-link active" href="darbuotojai.php" style="color:#0F5633;">PVC Darbuotojai</a></li>';
                            echo '<li class="nav-item"><a class="nav-link active" href="signup.php">Registruoti vartotoją</a></li>';
                            echo '<li class="nav-item"><a class="nav-link active" href="includes/logout.inc.php" style="color:#731620;">Atsijungti</a></li>';
                        } else {

                            echo '<li class="nav-item"><a class="nav-link active" href="login.php">Prisijungti</a></li>';
                        }
                        ?>


                    </ul>
                </div>
            </div>
        </nav>
    </div>