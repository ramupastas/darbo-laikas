$(document).ready(function () {
    $("#live_search").keyup(function () {
        var input = $(this).val();
        // alert(input);
        if (input != "") {
            $.ajax({
                url: "includes/livesearch.inc.php",
                method: "POST",
                data: {
                    input: input
                },
                success: function (data) {
                    $("#default_result").css("display", "none");
                    $("#search_result").html(data);
                }
            });
        } else {

            $("#search_result").css("display", "none");


        }
    });
});