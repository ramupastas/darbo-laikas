<?php
include "..\includes\db_conn.php";
include "select_darbo_vieta.php";
include "..\includes\ispejimai.php";

?>

<!DOCTYPE html>
<html lang="lt">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="styles.css">

    <title>Darbuotojų registracija</title>
</head>

<body>
    <nav class=" navbar navbar-light justify-content-center fs-3 mb-5">
        <h3>Darbo laiko registracija</h3>
    </nav>
    <?php

    echo $signalas; //is ispejimai.php


    ?>
    <div class="container">

        <div class="container d-flex justify-content-center">



            <form action="irasyti_duomenis.php" method="post" style="width:50vw; min-width:300px;">
                <div class="row mb-3 input-group-text">
                    <div class="col mb-3">

                        <label class="form-label">Skenuokite savo kodą:</label>
                        <input type="text" class="form-control" name="tabelio_id" id="tabelio_id" placeholder="" style="width:15vw; min-width:100px;" autofocus>
                    </div>
                    <div class="col mb-3">
                        <label class="form-label">Darbuotojas:</label>
                        <div class="container darbuotojas" id="darbuotojas">

                        </div>
                    </div>

                    <div class="input-group mb-3 darbo-vietos">
                        <label class="input-group" for="darbo_vietos">Darbo vieta:</label>

                        <select class="form-select" id="darbo_vietos" name="darbo_vietos">
                            <option value="" selected>....</option>
                            <?php
                            echo $option; //is select_darbo_vieta.php
                            ?>
                        </select>

                        <div class="input-group d-flex justify-content-center start-stop">

                            <div class="input-group justify-content-center irasyti">
                                <button class="btn btn-primary btn-lg" name="submit" type="submit">Įrašyti</button>

                            </div>

                        </div>

                    </div>

                </div>

            </form>
        </div>
    </div>
    <div id="sarasas" class="container justify-content-center">

    </div>


    <script src="darbu_sarasas.js"></script>
    <script src="tabelio_id.js"></script>
    <script src="..\timer.js"></script>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</body>

</html>