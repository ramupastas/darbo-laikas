<?php
// Suranda darbuotoja pagal ivesta tabelio numeri 
include "..\includes\db_conn.php";

if (isset($_POST['input'])) {
    $input = $_POST['input'];

    $sql = "SELECT * FROM `darbuotojai` WHERE `tabelio_id` =?";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location:index.php?fail_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "i", $input);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);

    if (mysqli_num_rows($result) > 0) {

        $row = mysqli_fetch_assoc($result);
        $darbuotojas = $row['darbuotojas'];
        echo "<input class= 'form-control' type='text' name='darbuotojas' value='$darbuotojas' readonly>";
    } else {
        echo "<input class= 'form-control' type='text' name='darbuotojas' value='0' readonly>";
        echo "<h5 class='text-danger text-center mt-3'>Tokio darbuotojo nėra </h5>";
    }
}
