<?php
include "..\includes\db_conn.php";

if (isset($_POST['submit'])) {
    date_default_timezone_set('Europe/Vilnius');
    $tabelio_id = intval($_POST['tabelio_id']);
    $darbuotojas = $_POST['darbuotojas'];
    $darbo_vieta = $_POST['darbo_vietos'];
    $darbo_pradzia = date('Y-m-d H:i:s');

    $sql = "SELECT * FROM `zurnalas`WHERE tabelio_id =? && (darbo_pabaiga IS NULL || darbo_pabaiga <= '2022-05-01 00:00:00') ORDER BY darbo_pradzia DESC";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location:index.php?fail_msg=SQL Klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $tabelio_id);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);

    if (mysqli_num_rows($result) > 0) {
        header("Location:index.php?fail_msg= Pirma reikia užbaigti pradėtą darbą!");
    } else {

        if (is_int($tabelio_id) && $tabelio_id != 0 && $darbuotojas != 0 && $darbo_vieta != "") {


            $sql = "INSERT INTO `zurnalas`(tabelio_id, darbuotojas, darbo_vieta, darbo_pradzia) VALUES (?,?,?,?); ";

            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt, $sql)) {
                header("Location:index.php?fail_msg= SQL stmt Klaida");
                exit();
            }
            mysqli_stmt_bind_param($stmt, "isss", $tabelio_id, $darbuotojas, $darbo_vieta, $darbo_pradzia);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            header("Location:index.php?success_msg=Informacija sėkmingai įrašyta!");
        } else {
            header("Location:index.php?fail_msg=Nepateikta visa teisinga informacija !");
        }
    }
}
