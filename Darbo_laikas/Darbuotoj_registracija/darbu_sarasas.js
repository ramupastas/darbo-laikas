// Funkcija paleidžia sarasas.php kodą ieškoti uzzregistruotu darbu duomenų bazėje pagal index.php  input id="tabelio_id" 
//ir įrašo rezultatą į index.php div id="sarasas" laukelį  
$(document).ready(function () {
    $("#tabelio_id").on('keyup', function () {

        var input = $(this).val().toLowerCase();

        if (input != "") {
            $.ajax({
                url: "sarasas.php",
                method: "POST",
                data: {
                    input: input
                },

                success: function (data) {
                    $("#sarasas").html(data);
                    $("#sarasas").css("display", "block");


                }
            });
        } else {
            $("#sarasas").css("display", "none");

        }
    });
});
