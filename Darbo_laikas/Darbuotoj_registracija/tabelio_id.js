
// Funkcija neleidžia barkodų skaneriui iš karto pateikti formos duomenis
$(document).ready(function () {
    $('#tabelio_id').on('keypress', function (event) {

        if (event.which == 13 || event.which == 10) {
            event.preventDefault();
        }
    });
});

// Funkcija paleidžia livesearch.php kodą ieškoti darbuotojo duomenų bazėje pagal index.php  input id="tabelio_id" 
//ir įrašo rezzultatą į index.php input id="darbuotojas" laukelį  
$(document).ready(function () {
    $("#tabelio_id").on('change', function () {

        var input = $(this).val().toLowerCase();

        if (input != "") {
            $.ajax({
                url: "livesearch.php",
                method: "POST",
                data: {
                    input: input
                },

                success: function (data) {
                    $("#darbuotojas").html(data);
                    $("#darbuotojas").css("display", "block");


                }
            });
        } else {
            $("#darbuotojas").css("display", "none");

        }
    });
});
