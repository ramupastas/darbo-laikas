<?php
include "..\includes\db_conn.php";

if (isset($_POST['input'])) {
    $input = $_POST['input'];


?>
    <table class="table table-hover table-striped text-center">
        <thead class="table-primary">
            <tr>

                <th scope="col">Tabelio numeris</th>
                <th scope="col">Darbuotojas</th>
                <th scope="col">Darbo vieta</th>
                <th scope="col">Darbo pradzia</th>
                <th scope="col">Darbo pabaiga</th>
                <th scope="col">Užbaigti darbą</th>
            </tr>
        </thead>
        <tbody>

            <?php
            //ieskome darbu lentoje zurnalas, kurie dar neuzbaigti
            $sql = "SELECT * FROM `zurnalas`WHERE tabelio_id =? && (darbo_pabaiga IS NULL || darbo_pabaiga <= '2022-05-01 00:00:00') ORDER BY darbo_pradzia DESC";

            $stmt = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmt, $sql)) {
                header("Location:index.php?fail_msg=SQL Klaida");
                exit();
            }
            mysqli_stmt_bind_param($stmt, "i", $input);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            mysqli_stmt_close($stmt);

            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {

                    $pradzia = $row['darbo_pradzia'];

            ?>
                    <h4 style='color: darkred;'>Yra neužbaigtų darbų!</h4>

                    <tr>
                        <td><?php echo $row['tabelio_id'] ?></td>
                        <td><?php echo $row['darbuotojas'] ?></td>
                        <td><?php echo $row['darbo_vieta'] ?></td>
                        <td><?php echo $pradzia ?></td>
                        <td><?php echo $row['darbo_pabaiga'] ?></td>

                        <td>
                            <a href="darbo_pabaiga.php?id=<?php echo $row['id']; ?>">

                                Užbaigti
                            </a>

                        </td>
                    </tr>

                <?php
                }
            } else {
                ?>
                <h4 style='color: darkgreen;'>Galima registruotis naujoje darbo vietoje</h4>
            <?php

            }

            ?>


        </tbody>
    </table>

<?php


}
