<?php
include "..\includes\db_conn.php";

if (isset($_GET['id'])) {

    $id = $_GET['id'];
    date_default_timezone_set('Europe/Vilnius');

    $sql = "SELECT * FROM `zurnalas` WHERE id = $id LIMIT 1";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    $tabelio_id = $row['tabelio_id'];
    $darbo_pradzia = strtotime($row['darbo_pradzia']);
    $darbo_pabaiga = strtotime(date('Y-m-d H:i:s'));
    $darbo_trukme = $darbo_pabaiga - $darbo_pradzia;


    $sql = "SELECT * FROM `darbuotojai` WHERE tabelio_id = $tabelio_id LIMIT 1";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    $darbuotojas = $row['darbuotojas'];

    $p1_nuo = strtotime($row['p1_pradzia']);
    $p1_iki = strtotime($row['p1_pabaiga']);

    $pietus_nuo = strtotime($row['pietus_pradzia']);
    $pietus_iki = strtotime($row['pietus_pabaiga']);

    $p2_nuo = strtotime($row['p2_pradzia']);
    $p2_iki = strtotime($row['p2_pabaiga']);


    if ($p1_nuo > $darbo_pradzia && $p1_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($p1_iki - $p1_nuo);
    }
    if ($pietus_nuo > $darbo_pradzia && $pietus_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($pietus_iki - $pietus_nuo);
    }
    if ($p2_nuo > $darbo_pradzia && $p2_iki <= $darbo_pabaiga) {
        $darbo_trukme = $darbo_trukme - ($p2_iki - $p2_nuo);
    }
    $darbo_pabaiga = date('Y-m-d H:i:s');
    $darbo_trukme = round($darbo_trukme / 3600, 2);

    $sql = "UPDATE `zurnalas` SET darbo_pabaiga=?, trukme=? WHERE id=? && (darbo_pabaiga IS NULL || darbo_pabaiga <= '2022-05-01 00:00:00') ";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: index.php?fail_msg=SQL klaida");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "sis", $darbo_pabaiga, $darbo_trukme, $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    header("Location: index.php?success_msg=Informacija sėkmingai įrašyta!");
}
