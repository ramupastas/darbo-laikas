<?php

$signalas = "";
if (isset($_GET['fail_msg'])) {
  $fail_msg = $_GET['fail_msg'];
  $signalas = '<div class="alert alert-danger fade show" role="alert" id="alert">
            ' . $fail_msg . '
           
          </div>';
} elseif (isset($_GET['success_msg'])) {
  $success_msg = $_GET['success_msg'];
  $signalas = '<div class="alert alert-success fade show" role="alert" id="alert">
            ' . $success_msg . '
           
          </div>';
} elseif (isset($_GET['warning_msg'])) {
  $warning_msg = $_GET['warning_msg'];
  $signalas = '<div class="alert alert-warning fade show" role="alert" id="alert">
            ' . $warning_msg . '
           
          </div>';
}
