<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Gamyba</title>
</head>

<body>
    <nav class=" navbar navbar-light justify-content-center fs-3 mb-5">
        <h3 class="text-muted">Gamybos darbo laiko apskaita</h3>

    </nav>

    <div class="container justify-content-center col-md-2">
        <div class="card bg-light mb-3" style="max-width: 40rem;">
            <div class="card-body">
                <h5 class="card-title mb-5 text-muted">Registracija darbo vietose</h5>
                <a class="btn btn-outline-primary btn-lg" href="Darbuotoj_registracija/index.php">Atidaryti</a>
            </div>
        </div>
        <div class="card bg-light mb-3" style="max-width: 40rem;">
            <div class="card-body">
                <h5 class="card-title mb-5 text-muted">Darbuotojų valdymas</h5>
                <a class="btn btn-outline-primary btn-lg" href="../Darbuotoju_valdymas/index.php">Atidaryti</a>
            </div>
        </div>
    </div>

</body>

</html>